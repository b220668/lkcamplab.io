---
title: "Home"
date: 2018-03-24T19:55:15-03:00
draft: false
---

{{< alerts info
"**Don't speak Portuguese**? click [here](/about)" 
>}}

O LKCAMP é um grupo de estudos de projetos de software livre, com foco no
kernel Linux, para todas as pessoas interessadas e sem pré-requisitos técnicos.
Temos encontros semanais, tanto virtualmente quanto presencialmente.

Para mais informações sobre esses encontros, visite
[Participando](/participando).

# Objetivos

Os objetivos do LKCAMP são:

- Disseminar conhecimentos do funcionamento e desenvolvimento do Kernel Linux ou
  outros projetos FOSS
- Ajudar pessoas interessadas a se tornarem contribuidoras do Kernel Linux ou
  outros projetos FOSS (principalmente no Brasil)
- Ajudar pessoas interessadas a serem remuneradas para trabalhar com o Kernel
  Linux ou outros projetos FOSS (principalmente no Brasil)

{{< alerts warning
"**Nota**: O LKCAMP surgiu no ambiente da Unicamp, e por isso existe bastante interação com seus alunos, porém não é restrito a esse ambiente de forma alguma. Todas as pessoas são mais que bem-vindas a participar."
>}}

# Edições anteriores

No passado o grupo tinha um formato diferente, centrado em apresentações sobre o
funcionamento do Kernel Linux. Informações sobre essas edições anteriores podem
ser vistas na página [Archived](/archived/).

# Ajude na nossa página

Todos somos voluntários e qualquer ajuda é bem-vinda. Nosso site é inteiramente aberto a contribuições, que podem ser feitas seguindo esse [guia](/posts/como-contribuir-para-este-site).
